# Vimdots

My personal configuration for the Vim Text Editor

## Features

+ A bunch of Pre-Installed Plugins (CoC, Nerd Tree, Vim-Airline etc.)

+ Easy customization since every (basic) aspect of Vim customization is in its
respective folder

+ Specific settings enabled through the use of `ftplugin`. This makes specific
settings enabled depending of the file type (mainly tab related adjustments,
conceallevel adjustments)

+ Custom functions in `functions.vim` under `settings/` that:

  + Automatically reindent HTML code heavily

  + Automatically enclose english words in `<bdo dir=ltr>` tags to help with
development of sites in RTL languages.

  + Automatically break HTML/CSS class attributes (one in each line) to increase
code legibility.

  + Automatically remove trailing whitespace.

+ Pre-sets:

  + vim-to-system-clipboard and the opposite

  + "x" deletes into blackhole register (i.e. does not yank)

  + Pasting without yanking

  + Ctrl-R and Ctrl-L to switch buffers left and right.

  + "Y" yanks until end-of-line

  + "X" deletes into black hole register until end-of-line

  + Color and kitty terminal specific color configuration (few lines of code)
