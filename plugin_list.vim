""       ____  __            _
""      / __ \/ /_  ______ _(_)___  _____
""     / /_/ / / / / / __ `/ / __ \/ ___/
""    / ____/ / /_/ / /_/ / / / / (__  )
""   /_/   /_/\__,_/\__, /_/_/ /_/____/
""                 /____/

call plug#begin('~/.vim/plugged')

"
"
" Theme and color plugins
"
"

"Themes for airline
Plug 'vim-airline/vim-airline-themes'

"Really cool status bar
Plug 'vim-airline/vim-airline'

"Sidebar for file navigation
Plug 'preservim/nerdtree'

"For coloring in devicons
Plug 'lambdalisue/glyph-palette.vim'
"Plug 'chriskempson/base16-vim'
Plug 'NLKNguyen/papercolor-theme'
Plug 'liuchengxu/space-vim-theme'
Plug 'joshdick/onedark.vim'
Plug 'tomasr/molokai'

"Fun plugin for showing minimap on the side
"Plug 'wfxr/minimap.vim'
"
"
" Git integration
"
"

"Git change indicators for vim
Plug 'airblade/vim-gitgutter'

"Function gitbranch#name() to show git branch inside vim
Plug 'itchyny/vim-gitbranch'

"
"
" Text manipulation helpers
"
"

"Syntax highlighting for Kotlin
"Plug 'udalov/kotlin-vim'

"coc plugin, autocomplete and syntax checking
Plug 'neoclide/coc.nvim', {'branch': 'release'}

"If spaces are indents make space indents have characters
"Plug 'Yggdroot/indentLine'

"Comment easier
Plug 'preservim/nerdcommenter'

"Alignment of text range with a reference character by using command: Tab/<character"
Plug 'godlygeek/Tabular'

"Autoswitch to US on normal mode, have lang on airline
"Plug 'lyokha/vim-xkbswitch'

"
"
" Web-Dev specific plugins
"
"

"Syntax for mustache and handlebars code
Plug  'jsit/vim-mustache-handlebars'

"highlight html-like tags
Plug 'Valloric/MatchTagAlways'

"Many language packs
Plug 'sheerun/vim-polyglot'

"
"
" Miscellanious
"
"

"Nice animation on scrolling with <C-f> and <C-b>
"Plug 'yuttie/comfortable-motion.vim'

"Devicons in nerdtree and airline
"NOTE: ALWAYS LOAD THIS LAST
Plug 'ryanoasis/vim-devicons'

call plug#end()
