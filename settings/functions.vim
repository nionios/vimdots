function FormatArHTML()
    "Match all english words (especially dates) and enclose them into a bdo for ltr
    "Current line only
    .,.s/\(\<[A-Za-z0-9,\.\/]*\>\)/<bdo dir="ltr">\1<\/bdo>/g
    echo 'Arabic formatted for HTML.'
endfunction

"Function for trimming the trailing whitespace.
"Yanked from: https://vim.fandom.com/wiki/Remove_unwanted_spaces
function TrimWhiteSpace()
    "Remove trailing whitespaces
    %s/\s*$//
    "Return cursor to previous position
    normal! ''
endfunction

"Function for totally evil reformat of an HTML file to my demented liking
function ReformatHTML()
    "Newline after opening tag
    %s/>/>\r/g
    "Newline before closing tag
    %s/<\//\r<\//g
    "Remove trailing whitespaces
    %s/\s*$//
    "Remove lines with nothing but newlines
    %s/^\n//
    "Reindent
    normal! gg=G
    "Return cursor to previous position
    normal ''
    echo 'Super evil HTML code reformat function completed. :)'
endfunction

"Function for totally evil reformat of a JS file to my demented liking
function ReformatJS()
    "Remove trailing whitespaces
    %s/\s*$//
    "Remove lines with nothing but newlines
    %s/^\n//
    "Reindent
    normal! gg=G
    "Return cursor to previous position
    normal ''
    echo 'Super evil JS code reformat function completed. :)'
endfunction

"Function to turn string concatenation to string interpolation in JS
function ConcatenationToInterpolation()
    .,.s/["']\(.*\)['"] *+ *\(.*\))/`\1${\2}`)/g
endfunction

"Function to break html attributes over multiple lines
function BreakAttrs()
    .,.s/\([a-zA-Z]=".\{-}"\) /\1\r/g
endfunction

function OpenInFirefox()
    !firefox % && echo 'File Opened in Firefox'
endfunction

"Open the current file on firefox
command Firefox :call OpenInFirefox()

nnoremap <C-X> :call BreakAttrs()<CR>
"Make the reindentation command remove traling white spaces too (whole file)
nnoremap gg=G :call TrimWhiteSpace()<CR> gg=G :echo 'Trailing whitespace removed, Indentation fixed.'<CR>
"Make a command for calling the ReformatHTML and FormatArHTML function easily
command Refhtml :call ReformatHTML()
command Refjs :call ReformatJS()
command Refar :call FormatArHTML()
command ConcatToInter :call ConcatenationToInterpolation()
