""       ___    _      ___
""      /   |  (_)____/ (_)___  ___
""     / /| | / / ___/ / / __ \/ _ \
""    / ___ |/ / /  / / / / / /  __/
""   /_/  |_/_/_/  /_/_/_/ /_/\___/

"Enabling airline with ascii
"let g:airline_symbols_ascii = 1
"Enabling airline with powerline (or Nerd) fonts
let g:airline_powerline_fonts = 1
"Show branch name in airline
let g:airline#extensions#branch#custom_head = 'gitbranch#name'
"
" Powerline symbols customization
"

" Guard
if !exists('g:airline_symbols')
    let g:airline_symbols = {}
endif

let g:airline_symbols.dirty=''
let g:airline_symbols.maxlinenr = ' '
let g:airline_left_sep = ''
let g:airline_left_alt_sep = ''
let g:airline_right_sep = ''
let g:airline_right_alt_sep = ''

"Enabling airline for a better looking tabline
let g:airline#extensions#tabline#enabled = 1

"Tab and airline look
"let g:airline#extensions#tabline#left_sep = ' '
"let g:airline#extensions#tabline#left_alt_sep = ' '
"let g:airline#extensions#tabline#right_sep = ''
"let g:airline#extensions#tabline#right_alt_sep = ''

let g:airline#extensions#tabline#show_close_button = 1
"let g:airline#extensions#tabline#close_symbol = '✖'
"NOTE: Airline Theme is mentioned in ../colors.vim
