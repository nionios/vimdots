""       __ __           __    _           ___
""      / //_/__  __  __/ /_  (_)___  ____/ (_)___  ____ ______
""     / ,< / _ \/ / / / __ \/ / __ \/ __  / / __ \/ __ `/ ___/
""    / /| /  __/ /_/ / /_/ / / / / / /_/ / / / / / /_/ (__  )
""   /_/ |_\___/\__, /_.___/_/_/ /_/\__,_/_/_/ /_/\__, /____/
""             /____/                            /____/

"For misclicks
:command WQ wq
:command Wq wq
:command W w
:command Q q

"Make Y yank till end of line
nnoremap Y y$
"Make x not yank, just delete (by yanking to black hole register)
nnoremap x "_x
"Make X send rest of line to blackhole register
nnoremap X "_D

"For switching buffers
noremap <C-h> :bprev<CR>
noremap <C-l> :bnext<CR>

"Quickly display hybrid line numbers or not (normal mode only)
nnoremap <C-y> :set number! relativenumber!<CR>
